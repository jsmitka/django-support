from django import forms

# Create your models here.

from models import Answer, Question

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        exclude = ("question", "like", "dislike")

class CreateQuestionForm(forms.ModelForm):
    class Meta:
        model = Question