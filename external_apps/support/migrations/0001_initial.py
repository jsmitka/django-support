# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Asset'
        db.create_table('support_asset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('support', ['Asset'])

        # Adding model 'Question'
        db.create_table('support_question', (
            ('asset_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['support.Asset'], unique=True, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('support', ['Question'])

        # Adding model 'Answer'
        db.create_table('support_answer', (
            ('asset_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['support.Asset'], unique=True, primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['support.Question'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('like', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('dislike', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('support', ['Answer'])


    def backwards(self, orm):
        
        # Deleting model 'Asset'
        db.delete_table('support_asset')

        # Deleting model 'Question'
        db.delete_table('support_question')

        # Deleting model 'Answer'
        db.delete_table('support_answer')


    models = {
        'support.answer': {
            'Meta': {'object_name': 'Answer', '_ormbases': ['support.Asset']},
            'asset_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['support.Asset']", 'unique': 'True', 'primary_key': 'True'}),
            'dislike': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'like': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['support.Question']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'support.asset': {
            'Meta': {'object_name': 'Asset'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'support.question': {
            'Meta': {'object_name': 'Question', '_ormbases': ['support.Asset']},
            'asset_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['support.Asset']", 'unique': 'True', 'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['support']
