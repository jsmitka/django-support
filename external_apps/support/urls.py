from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template


urlpatterns = patterns('',
    url(r'^all/$',
        'support.views.all',
        name="support.all", ),
    url(r'^create/$',
        'support.views.create',
        name="support.create", ),
    url(r'^detail/(?P<question_pk>\d+)/$',
        'support.views.detail',
        name="support.detail", ),
)