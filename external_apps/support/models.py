from django.db import models


class Asset(models.Model):
    created = models.DateTimeField(auto_now_add=True)

class Question(Asset):
    title = models.CharField(max_length=255)
    text = models.TextField(blank=True, null=True)


class Answer(Asset):
    question = models.ForeignKey(Question)

    text = models.TextField()

    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)