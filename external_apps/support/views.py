import datetime

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from models import Question, Answer
from forms import AnswerForm, CreateQuestionForm


def all(request, template='support/all.html'):
    questions = Question.objects.all().order_by("-created")
    return render_to_response(template,
                              {"questions": questions},
                              context_instance=RequestContext(request))

def detail(request, question_pk, template='support/detail.html'):
    question = Question.objects.get(pk=question_pk)
    answers = question.answer_set.all().order_by("created")
    form = AnswerForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.question = question
        obj.save()
        return HttpResponseRedirect("")
    return render_to_response(template,
                              {"question": question, "answers": answers, "form": form, },
                              context_instance=RequestContext(request))


def create(request, template='support/create.html'):
    form = CreateQuestionForm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        return HttpResponseRedirect(reverse("support.detail", args=[obj.pk]))
    return render_to_response(template,
                              {"form": form, },
                              context_instance=RequestContext(request))