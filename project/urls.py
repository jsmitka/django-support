# -*- coding: utf-8 -*-
# Django void v.1.0 for Django 1.4
# author:   Ondrej Sika
#           sika.ondrej@gmail.com
#           http://ondrejsika.com

from django.conf.urls import patterns, include, url
from django.conf import settings

from _urls import *

urlpatterns += patterns('',
    url(r'^support/', include("support.urls")),
)
